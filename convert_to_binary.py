# Won't work with floats

def con_bin(num):
    if num < 0:
        is_neg = True
        num = abs(num)
    else:
        is_neg = False
    result = ""
    if num == 0:
        return "0"
    while num != 0:
        result = str(num % 2) + result
        num //= 2
    if is_neg:
        result = "-" + result
    return result

num = int(input("Enter a number: "))
print("{0} in binary is {1}".format(num, con_bin(num)))