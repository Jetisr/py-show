bottles_of_beer = 99
bottle = "bottles"

while bottles_of_beer > 0:
    print("{0} bottles of beer on the wall, {0} {1} of beer."
          .format(bottles_of_beer, bottle))
    bottles_of_beer -= 1
    if bottles_of_beer == 1:
        bottle = "bottle"
    if bottles_of_beer != 0:
        print("Take one down and pass it around, {0} {1} of beer on the wall."
              .format(bottles_of_beer, bottle))
    else:
        print("""
Take one down and pass it around, no more bottles of beer on the wall.
        """)

print("""
No more bottles of beer on the wall, no more bottles of beer.
Go to the store and buy some more, 99 bottles of beer on the wall.
    """)
