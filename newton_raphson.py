# Implementation of the Newton-Raphson algorithm for determinging square roots

epsilon = 0.01
y = 24.0
guess = y / 2.0
num_guesses = 0

while abs((guess ** 2) - y) >= epsilon:
    num_guesses += 1
    guess -= ((guess ** 2) - y) / (2 * guess)
print(
    """
Number of guesses: {0}
Square root of {1} is about {2}
    """.format(
        num_guesses, y, guess
        )
    )
