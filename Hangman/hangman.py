#!/usr/bin/env python3

"""Hangman game."""

import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Return a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    in_file = open(WORDLIST_FILENAME, 'r')
    line = in_file.readline()
    loaded_words = line.split()
    print(("  "), len(loaded_words), "words loaded.")
    return loaded_words


def choose_word(list_to_choose_from):
    """
    list_to_choose_from (list): list of words (strings).

    Returns a word from list_to_choose_from at random
    """
    return random.choice(list_to_choose_from)

WORD_LIST = load_words()


def is_word_guessed(word, letters_guessed):
    """
    word: string, the word the user is guessing.

    letters_guessed: list, what letters have been guessed so far

    returns: boolean, True if all the letters of word are in
    letters_guessed;
      False otherwise
    """
    length_of_word = len(word)
    correct_letters = 0
    wrong_letters = 0
    for letter in letters_guessed:
        if letter in word:
            correct_letters += 1
        else:
            wrong_letters += 1
    return bool(correct_letters == length_of_word)


def get_guessed_word(word, letters_guessed):
    """
    word: string, the word the user is guessing.

    letters_guessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in word have been guessed so far.
    """
    correct_letters = []
    guessed_word = []
    for letter in letters_guessed:
        if letter in word:
            correct_letters.append(letter)
    for letter in word:
        if letter not in correct_letters:
            guessed_word.append("_ ")
        else:
            guessed_word.append(letter + " ")
    return "".join(guessed_word)


def get_available_letters(letters_guessed):
    """
    letters_guessed: list, what letters have been guessed so far.

    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    """
    available_letters = string.ascii_lowercase
    for letter in letters_guessed:
        available_letters = available_letters.replace(letter, "")
    return available_letters


def hangman(word):
    """
    word: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many
      letters the word contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the
      partially guessed word so far, as well as letters that the
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    """
    sw_length = len(word)

    guesses = 8
    letters_guessed = []

    print("Welcome to the game, Hangman!")
    print("I am thinking of a word that is {} letters long".format(sw_length))
    print("---------")

    while guesses > 0:
        print("You have {} guesses left".format(guesses))
        print(
            "Available letters: {}".format(
                get_available_letters(letters_guessed)
            )
        )
        guess = input("Please guess a letter: ")
        guessed_word = get_guessed_word(word, letters_guessed + list(guess))
        guessed_word_str = "".join(guessed_word).replace(" ", "")
        if guess in letters_guessed:
            print(
                "Oops! You've already guessed that letter: {}".format(
                    guessed_word
                )
            )
        elif guess not in word:
            print(
                "Oops! That letter is not in my word: {}".format(guessed_word)
            )
            guesses -= 1
        elif guess in word:
            print(
                "Good guess: {}".format(guessed_word)
            )
        print("---------")
        letters_guessed.append(guess)
        if guessed_word_str == word:
            break
    if guesses == 0:
        print("Sorry, you ran out of guesses. The word was {}".format(word))
    else:
        print("Congratulations, you won!")

SECRET_WORD = choose_word(WORD_LIST).lower()
hangman(SECRET_WORD)
