def square(num):
    """Returns the square root of the given number."""
    epsilon = 0.01
    low = 0.0
    high = num
    ans = (high + low) / 2

    while abs((ans ** 2) - num) >= epsilon:
        if ans ** 2 < num:
            low = ans
        else:
            high = ans
        ans = (high + low) / 2.0

    return ans


def cube(num):
    """Returns the cube root of the given number."""
    epsilon = 0.01
    low = 0.0
    high = num
    ans = (high + low) / 2

    while abs((ans ** 3) - num) >= epsilon:
        if ans ** 3 < num:
            low = ans
        else:
            high = ans
        ans = (high + low) / 2.0
    return ans

print("Square root of 92 is ~{0}".format(square(92)))
print("Cube root of 92 is ~{0}".format(cube(92)))
